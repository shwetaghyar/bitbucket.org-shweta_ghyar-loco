# The RESTful web service for managing transactions

which store transactions and get the data for requested transaction Id, type,etc
##Git clone :
 git clone https://shwetaghyar@bitbucket.org/shwetaghyar/bitbucket.org-shweta_ghyar-loco.git

## Run the server
1. Go to the project folder
2. Run command : sudo npm install
3. Run command node server.js
4. Server will be running on port 3000

## Run the tests using command 
 : mocha Test/TransactionHandlerTest.js

## API's to call

#### 1. createTransaction(PUT) :-> Create transaction with the specified transaction id and the data.
    URL : http://localhost:3000/transactionservice/transaction/{{transaction_id}}
    body :  { "amount": 5000, "type":"cars", "parent_id": 12345 }
    Headers : Content-Type : application/json

#### 2. getTransactionById(GET) :-> Get the transactions for the requested transaction ID
    URL : http://localhost:3000/transactionservice/transaction/{{transaction_id}}

#### 3. getTransactionByType(GET) :-> Get the transactions for the requested transaction type
    URL : http://localhost:3000/transactionservice/transaction/{{type}}

#### 4. getSumOfTransactionsById(GET) :-> Get the sum of transaction amount for the requested transactionId matching with the parentID of other transactions
    URL : http://localhost:3000/transactionservice/sum/{{transaction_id}}