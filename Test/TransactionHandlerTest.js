let chai = require('chai');
let sinon = require('sinon');
const mockery = require('mockery');
let should = chai.should();
let transactionHandler = require("../routes/TransactionHandler");
let transactionService = require('../services/TransactionService');
describe('Test createTransaction', function () {
  beforeEach(function (done) {
    createTransactionStub = sinon.stub(transactionService, "createTransaction");
    done();
  });
  afterEach(function (done) {
    createTransactionStub.restore();
    done();
  });

  it('should return success if the transaction id is not already present', function (done) {
    var req, resp;
    createTransactionStub.resolves({ success: true });
    req = {
      params: {
        transaction_id: 456
      },
      body: {
        "amount": 10000, "type": "cars", "parent_id": 111
      }
    }
    resp = {
      send: function (result) {
        result.status.should.be.equal('ok');
        done();
      },
      status: function (err) {
        err.should.be.equal(500);
        return this;
      }
    }

    transactionHandler.createTransaction(req, resp);

  })
  it('should return status 500 if the transaction id is  already present', function (done) {
    var req, resp;

    createTransactionStub.rejects({ success: false, msg: "transaction id already exists." });
    req = {
      params: {
        transaction_id: 456
      },
      body: {
        "amount": 10000, "type": "cars", "parent_id": 123
      }
    }
    resp = {
      status: function (err) {
        err.should.be.equal(500);
        return this;
      },
      send: function (result) {
        result.success.should.be.equal(false);
        result.msg.should.be.equal("transaction id already exists.");
        done();
      },

    }

    transactionHandler.createTransaction(req, resp);

  })
  it('should return 400 if transactionId is not a number', function (done) {
    var req, resp;
    req = {
      params: {
        transaction_id: "testId"
      },
      body: {
        "amount": 10000, "type": "cars", "parent_id": 123
      }
    }
    resp = {
      status: function (err) {
        err.should.be.equal(400);
        return this;
      },
      send: function (result) {
        result.status.should.be.equal("failed");
        result.msg.should.be.equal("Invalid query parameters. Transaction id should be a number.");
        done();
      },

    }

    transactionHandler.createTransaction(req, resp);

  })
  it('should return 400 if body is not sent', function (done) {
    var req, resp;
    req = {
      params: {
        transaction_id: "testId"
      }
    }
    resp = {
      status: function (err) {
        err.should.be.equal(400);
        return this;
      },
      send: function (result) {
        result.status.should.be.equal("failed");
        result.msg.should.be.equal("Invalid parameters ");
        done();
      },

    }
    transactionHandler.createTransaction(req, resp);
  })
  it('should return 400 if amount is not a number', function (done) {
    var req, resp;
    req = {
      params: {
        transaction_id: 123
      },
      body: {
        "amount": "amount", "type": "cars", "parent_id": 123
      }
    }
    resp = {
      status: function (err) {
        err.should.be.equal(400);
        return this;
      },
      send: function (result) {
        result.status.should.be.equal("failed");
        result.msg.should.be.equal("The amount should be a number.");
        done();
      },

    }
    transactionHandler.createTransaction(req, resp);
  })
  it('should return 400 if type is not a string', function (done) {
    var req, resp;
    req = {
      params: {
        transaction_id: 123
      },
      body: {
        "amount": 1000, "type": 123, "parent_id": 123
      }
    }
    resp = {
      status: function (err) {
        err.should.be.equal(400);
        return this;
      },
      send: function (result) {
        result.status.should.be.equal("failed");
        result.msg.should.be.equal("The type should be a string.");
        done();
      },
    }
    transactionHandler.createTransaction(req, resp);
  })
  it('should return 400 if parentId is not a number', function (done) {
    var req, resp;
    req = {
      params: {
        transaction_id: 123
      },
      body: {
        "amount": 1000, "type": "type", "parent_id": "parentId"
      }
    }
    resp = {
      status: function (err) {
        err.should.be.equal(400);
        return this;
      },
      send: function (result) {
        result.status.should.be.equal("failed");
        result.msg.should.be.equal("The parent id should be a number.");
        done();
      },
    }
    transactionHandler.createTransaction(req, resp);
  })
})
describe('Test getTransactionById', function () {
  beforeEach(function (done) {
    getTransactionByIdStub = sinon.stub(transactionService, "getTransactionById");
    done();
  });
  afterEach(function () {
    getTransactionByIdStub.restore(); // Unwraps the spy
  });
  it('should return success if the transaction id is present', function (done) {
    var req, resp;
    getTransactionByIdStub.resolves({ "amount": 10000, "type": "cars", "parent_id": 111, "transactionId": 123 });
    req = {
      params: {
        transaction_id: 123
      }
    }
    resp = {
      send: function (result) {
        result.should.not.be.equal(undefined);
        result.transactionId.should.be.equal(123);
        done();
      }
    }
    transactionHandler.getTransactionById(req, resp);
  })

  it('should return 500 if the transaction id is not found.', function (done) {
    var req, resp;
    getTransactionByIdStub.rejects({ success: false, msg: "Transaction not found for the given Id" });
    req = {
      params: {
        transaction_id: 456
      }
    }
    resp = {
      status: function (err) {
        err.should.be.equal(500);
        return this;
      },
      send: function (result) {
        result.success.should.be.equal(false);
        result.msg.should.be.equal("Transaction not found for the given Id");
        done();
      }
    }
    transactionHandler.getTransactionById(req, resp)
  })
  it('should return 400 if the transaction id is string.', function (done) {
    var req, resp;
    req = {
      params: {
        transaction_id: "sampleId"
      }
    }
    resp = {
      status: function (err) {
        err.should.be.equal(400);
        return this;
      },
      send: function (result) {
        result.status.should.be.equal("failed");
        result.msg.should.be.equal("Invalid query parameters. Transaction id should be a number.");
        done();
      }
    }
    transactionHandler.getTransactionById(req, resp)
  })
})
describe('Test getSumOfTransactionsById', function () {
  beforeEach(function (done) {
    getSumOfTransactionsByIdStub = sinon.stub(transactionService, "getSumOfTransactionsById");
    done();
  });
  afterEach(function (done) {
    getSumOfTransactionsByIdStub.restore(); // Unwraps the spy
    done();
  });
  it('should return success if the transaction id  and parentId matches', function (done) {
    var req, resp;
    getSumOfTransactionsByIdStub.resolves({ "sum": 100000 });
    req = {
      params: {
        transaction_id: 123
      }
    }
    resp = {
      send: function (result) {
        result.should.not.be.equal(undefined);
        result.sum.should.not.be.equal(undefined);
        done();
      }
    }
    transactionHandler.getSumOfTransactionsById(req, resp);
  })
  it('should return failure if the transaction id and parentId doesnt match', function (done) {
    var req, resp;
    getSumOfTransactionsByIdStub.rejects({ success: false, msg: "Transaction not found with parentId equal to given transactionId" });
    req = {
      params: {
        transaction_id: 123
      }
    }
    resp = {
      status: function (err) {
        err.should.be.equal(500);
        return this;
      },
      send: function (result) {
        result.should.not.be.equal(undefined);
        result.success.should.not.be.equal(undefined);
        done();
      }
    }
    transactionHandler.getSumOfTransactionsById(req, resp);
  })
  it('should return failure if the transaction id is not a number', function (done) {
    var req, resp;
    getSumOfTransactionsByIdStub.rejects({ success: false, msg: "Transaction not found with parentId equal to given transactionId" });
    req = {
      params: {
        transaction_id: "testId"
      }
    }
    resp = {
      status: function (err) {
        err.should.be.equal(400);
        return this;
      },
      send: function (result) {
        result.should.not.be.equal(undefined);
        result.status.should.be.equal("failed");
        result.msg.should.be.equal("Invalid query parameters. Transaction id should be a number.");
        done();
      }
    }
    transactionHandler.getSumOfTransactionsById(req, resp);
  })
})