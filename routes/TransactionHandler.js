const logger = require('log4js').getLogger("TransactionHandler"),
  transactionService = require('../services/TransactionService');
/**
 * API used to create new transaction
 * @param {*} req 
 * @param {*} res 
 */
exports.createTransaction = (req, res) => {
  if (!req.body || !req.body.amount || !req.body.type) {
    res.status(400).send({ "status": "failed", "msg": "Invalid parameters " });
    return;
  }
  logger.trace("createTransaction recieved body: ", req.body);
  let transaction = req.body;
  //Check if the transaction id is a number
  if(isNaN(parseInt(req.params.transaction_id))){
    res.status(400).send({ "status": "failed", "msg": "Invalid query parameters. Transaction id should be a number." });
    return;
  }
  //Check if the amount is number
  if(typeof req.body.amount !== "number"){
    res.status(400).send({ "status": "failed", "msg": "The amount should be a number." });
    return;
  }
  //Check if the type is string
  if(typeof req.body.type !== "string"){
    res.status(400).send({ "status": "failed", "msg": "The type should be a string." });
    return;
  }
  //check if parent id is long only if its sent(as its given as optional parameter)
  if(req.body.parent_id && typeof req.body.parent_id !== "number"){
    res.status(400).send({ "status": "failed", "msg": "The parent id should be a number." });
    return;
  }
  let transactionId = parseInt(req.params.transaction_id);
  transactionService.createTransaction(transactionId, transaction).then(data => {
    res.send({ "status": "ok" });
    return;
  }).catch(err => {
    res.status(500).send(err);
    return;
  });
}
/**
 * API to get transaction data for given Id
 * @param {*} req 
 * @param {*} res 
 */
exports.getTransactionById = (req, res) => {
  logger.trace("getTransactionById called for id : ", req.params.transaction_id);
    //Check if the transaction id is a number
    if(isNaN(parseInt(req.params.transaction_id))){
      res.status(400).send({ "status": "failed", "msg": "Invalid query parameters. Transaction id should be a number." });
      return;
    }
  let transactionId = parseInt(req.params.transaction_id);
  transactionService.getTransactionById(transactionId).then(data => {
    res.send(data);
    return;
  }).catch(err => {
    res.status(500).send(err)
    return;
  })
}

/**
 * API to get transactions for given type
 * @param {*} req 
 * @param {*} res 
 */
exports.getTransactionsByType = (req, res) => {
  logger.trace("getTransactionById called for type: ", req.params.type);
  let transactionType = req.params.type;
    //Check if the type is string
  if(typeof req.params.type !== "string"){
    res.status(400).send({ "status": "failed", "msg": "The type should be a string." });
    return;
  }
  transactionService.getTransactionByType(transactionType).then(data => {
    res.send(data);
    return;
  }).catch(err => {
    res.status(500).send(err)
    return;
  })
}

/**
 * get sum of transaction amounts for which the transaction Id is the parent Id
 * @param {*} req 
 * @param {*} res 
 */
exports.getSumOfTransactionsById = (req, res) => {
  logger.trace("getSumOfTransactionsById called for id: ", req.params.transaction_id);
    //Check if the transaction id is a number
    if(isNaN(parseInt(req.params.transaction_id))){
      res.status(400).send({ "status": "failed", "msg": "Invalid query parameters. Transaction id should be a number." });
      return;
    }
  let transactionId = parseInt(req.params.transaction_id);
  transactionService.getSumOfTransactionsById(transactionId).then(data => {
    res.send(data);
    return;
  }).catch(err => {
    res.status(500).send(err)
    return;
  })
}


