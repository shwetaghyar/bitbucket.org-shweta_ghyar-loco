const transactionHandler = require('./TransactionHandler');
module.exports = function (app) {
  app.put('/transactionservice/transaction/:transaction_id', transactionHandler.createTransaction);
  app.get('/transactionservice/transaction/:transaction_id', transactionHandler.getTransactionById);
  app.get('/transactionservice/types/:type', transactionHandler.getTransactionsByType);
  app.get('/transactionservice/sum/:transaction_id', transactionHandler.getSumOfTransactionsById);
};
