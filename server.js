const log4js = require('log4js');
const logger = require('log4js').getLogger("TransactionDemo");

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3000
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

require('./routes/routes')(app);

app.listen(port, () => {
  logger.info(`Example app listening on port ${port}!`)
})
