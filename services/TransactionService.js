var log4js = require('log4js');
var logger = log4js.getLogger("TransactionService");
//Initialise an array to store all transactions
var transactions = [];


exports.createTransaction = createTransaction;
exports.getTransactionById = getTransactionById;
exports.getTransactionByType = getTransactionByType;
exports.getSumOfTransactionsById = getSumOfTransactionsById;

/**
 * create new transaction with the given transaction id and data given in the body
 * @param {*} transId Transaction id with which the transaction is to be created
 * @param {*} transData Transaction data to be inserted
 */
function createTransaction(transId, transData) {
  logger.trace("In createTransaction the transaction data is: ", { transId: transId, transData: transData });
  return new Promise((resolve, reject) => {
    //Check if the id already exists
    let obj = transactions.find(obj => obj.transactionId == transId);
    if (!!obj) {
      reject({ success: false, msg: "transaction id already exists." });
      return;
    } else {
      transData.transactionId = transId;
      transactions.push(transData);
      logger.info("Transaction created successfully for id; ", transId);
      resolve({ success: true })
      return;
    }
  });
}
/**
 * Get transaction data for the specified transaction id
 * @param {*} transId Transaction id for which the transaction data is to be retrieved
 */
function getTransactionById(transId) {
  logger.trace("In getTransactionById data is: ", { transId: transId });
  return new Promise((resolve, reject) => {
    //Match the given transaction id with the transaction data
    let obj = transactions.find(obj => obj.transactionId == transId);
    if (!!obj) {
      return resolve(obj);
    } else {
      return reject({ success: false, msg: "Transaction not found for the given Id" });
    }
  })
}
/**
 * Get transactions of specified type
 * @param {String} transType The type for which the transactions to be retrieved
 */
function getTransactionByType(transType) {
  logger.trace("In getTransactionByType data is: ", { transType: transType });
  return new Promise((resolve, reject) => {
    //Match the given transaction type with the transaction data
    let obj = transactions.filter(obj => obj.type === transType);
    let transactionList = obj.map(transaction => transaction.transactionId);
    if (!!obj && transactionList.length) {
      resolve( transactionList);
      return;
    } else {
      reject({ success: false, msg: "Transaction not found for the given type" });
      return;
    }
  })
}
/**
 * Get sum of transaction amounts for which the requested id is parent_id
 * @param {*} transId transaction Id to be matched with parent_id
 */
function getSumOfTransactionsById(transId) {
  logger.trace("In getSumOfTransactionsById data is: ", { transId: transId });
  return new Promise((resolve, reject) => {
    //Match the given transaction id with the parent_id of the transaction data
    let obj = transactions.filter(obj => obj.parent_id == transId);
    let amountList = obj.map(transaction => transaction.amount);
    if (!!obj && amountList.length) {
      let sum = amountList.reduce(function (a, b) { return a + b; }, 0);
      resolve({ "sum": sum });
      return;
    } else {
      reject({ success: false, msg: "Transaction not found with parentId equal to given transactionId" });
      return;
    }
  })
}


